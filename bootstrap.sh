# SETUP FOR VAGRANT DEV BOX BUZZHIRE
PROJNAME=potato

# GENERAL SETUP
sudo apt-get update
sudo apt-get -y install vim
sudo apt-get -y install curl
sudo apt-get -y install python-pip
sudo apt-get -y install python-dev
sudo apt-get -y install unzip
sudo apt-get -y install git
cd /vagrant/ && ./install_deps
echo "DONE"
